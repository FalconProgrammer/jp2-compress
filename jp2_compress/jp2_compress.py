#!/usr/bin/env python

import os, argparse, p_tqdm, shutil
from pathlib import Path
from PIL import Image as PImage
import tempfile
from typing import Optional


def test_image_jp2recomp(image_name: str, target_filesize: int, tmp_dir: Path, start_comp_level: int, output_folder: Optional[Path] = None) -> int:

	tmp_image = PImage.open(image_name)

	# NOTE (JB) Get temporary filename for testing compression
	tmp_name = str(tmp_dir.joinpath(next(tempfile._get_candidate_names()) + ".jp2"))

	# NOTE (JB) Initial values
	comp_level = start_comp_level
	file_size = 60

	# NOTE (JB) Test comp levels
	while file_size >= target_filesize:
		tmp_image.save(tmp_name, quality_layers=[comp_level])

		file_size = os.path.getsize(tmp_name) / 1024

		if file_size >= target_filesize:
			comp_level += 1

	del tmp_image

	# NOTE (JB) Keep file for temporary viewint
	if output_folder:
		tmp_out_file = str(output_folder.joinpath(os.path.splitext(os.path.split(image_name)[1])[0] + ".jp2"))
		shutil.copyfile(tmp_name, tmp_out_file)

	os.remove(tmp_name)

	return comp_level


def main() -> int:

	parser = argparse.ArgumentParser()
	parser.add_argument("input_folder")
	parser.add_argument("--output-folder", type=str)
	parser.add_argument("--target-filesize", default=20, type=int)
	parser.add_argument("--start-comp-level", default=1, type=int)

	args = parser.parse_args()

	image_path = Path(args.input_folder)

	# NOTE (JB) Cycle through all files and determine best compression level
	all_images = [str(i) for i in image_path.glob("*.*") if i.is_file()]
	all_images.sort()

	temporary_directory = tempfile.TemporaryDirectory(suffix="fm-comp-checker")
	tmp_dir = Path(temporary_directory.name)

	start_comp_level = args.start_comp_level

	if args.output_folder:
		output = Path(args.output_folder)

		if not output.exists():
			output.mkdir(parents=True)
		else:
			u_i = input("WARNING: Output directory [ {} ] already exists. Will overwrite existing files if continue. [Y/n] ".format(output))

			if u_i == "n":
				return 0

		comp_levels = p_tqdm.p_map(lambda x: test_image_jp2recomp(x, args.target_filesize, tmp_dir, start_comp_level, Path(args.output_folder)), all_images)
	else:
		comp_levels = p_tqdm.p_map(lambda x: test_image_jp2recomp(x, args.target_filesize, tmp_dir, start_comp_level), all_images)

	print("All compression Levels:")
	for i, c in zip(all_images, comp_levels):
		print("\t{}\t-> {}".format(i, c))

	print("Outputting details to [ complevels.csv ]")
	with open("complevels.csv", "w") as writer:
		writer.write("Image,\"Compression Level\",\n")
		for i, c in zip(all_images, comp_levels):
			writer.write("{},{},\n".format(i,c))

	return 0


if __name__ == '__main__':
    exit(main())
